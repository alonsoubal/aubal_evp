
package root.model.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "language",
    "lexicalEntries",
    "type",
    "word"
})
public class Result {

    @JsonProperty("id")
    public String id;
    @JsonProperty("language")
    public String language;
    @JsonProperty("lexicalEntries")
    public List<LexicalEntry> lexicalEntries = new ArrayList<LexicalEntry>();
    @JsonProperty("type")
    public String type;
    @JsonProperty("word")
    public String word;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Result withId(String id) {
        this.id = id;
        return this;
    }

    public Result withLanguage(String language) {
        this.language = language;
        return this;
    }

    public Result withLexicalEntries(List<LexicalEntry> lexicalEntries) {
        this.lexicalEntries = lexicalEntries;
        return this;
    }

    public Result withType(String type) {
        this.type = type;
        return this;
    }

    public Result withWord(String word) {
        this.word = word;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Result withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
