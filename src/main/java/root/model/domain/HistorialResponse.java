/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mono
 */
public class HistorialResponse {
    List<ResultResponse> historial = new ArrayList<>();

    public List<ResultResponse> getHistorial() {
        return historial;
    }

    public void setHistorial(List<ResultResponse> historial) {
        this.historial = historial;
    }
    
    
}
