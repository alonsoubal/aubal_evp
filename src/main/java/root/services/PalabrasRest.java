package root.services;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.model.dao.PalabrasDAO;
import root.model.entities.Palabras;

@Path("/palabras")
public class PalabrasRest {

    PalabrasDAO dao = new PalabrasDAO();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {
        List<Palabras> lista = dao.findPalabrasEntities();
        System.out.println("Listado de palabras del diccionario");
        return Response.ok(200).entity(lista).build();

    }

    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") String idbuscar) {
        Palabras palabra = dao.findPalabras(idbuscar);
        System.out.println("Palabra ingresada correctamente");
        return Response.ok(200).entity(palabra).build();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response nuevo(Palabras PalabraNueva) throws Exception {
        PalabrasDAO palabraNueva = new PalabrasDAO();
        palabraNueva.create(PalabraNueva);
        return Response.ok("La Palabra ingresada correctamente").build();
    }
    
     @PUT
    public Response actualizar(Palabras actualizar) throws Exception {
        String resultado = null;
        PalabrasDAO palabra = new PalabrasDAO();
        palabra.edit(actualizar);
        return Response.ok("La Palabra fue modificada con éxito en el diccionario...").build();
    }

    @DELETE
    @Path("/{iddelete}")
    public Response eliminarId(@PathParam("iddelete") String iddelete) throws Exception {
        PalabrasDAO palabra = new PalabrasDAO();
        palabra.destroy(iddelete);
        return Response.ok("Palabra fue eliminada del diccionario... ").build();
    }
    
    
}
