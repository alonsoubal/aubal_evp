package root.services;

import java.io.IOException;
import java.util.logging.Logger;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import root.model.api.OxfordDef;

import com.google.gson.*;
import java.util.logging.Level;

public class ApiOxford {

    private static final Logger log = Logger.getLogger(ApiOxford.class.getName());

    private final String prefixUrl = "https://od-api.oxforddictionaries.com/api/v2/entries/es/";
    private final String suffixUrl = "?fields=definitions&strictMatch=false";
    private final OkHttpClient client = new OkHttpClient();
    private final Gson gson = new Gson();
    
    public OxfordDef getDefinitions(String word) throws IOException {

        //log.info("Environment::" + gson.toJson(System.getProperties()));
        String app_id = System.getProperty("OXFORD_APP_ID");
        String app_key = System.getProperty("OXFORD_APP_KEY");
      
        log.log(Level.INFO, "Request::app_id::{0}", app_id);
        log.log(Level.INFO, "Request::app_key::{0}", app_key);
        log.log(Level.INFO,"Request::Url" + prefixUrl + "Request::Url" + suffixUrl, word);

        Request request = new Request.Builder()
                                        .url(prefixUrl + word + suffixUrl)
                                        .get()
                                        .addHeader("accept", "application/json")
                                        .addHeader("app_id", "49b0e4aa")
                                        .addHeader("app_key", "b9ef15bac5378475715f6043bb27af54")
                                        //.addHeader("app_if", app_id)
                                        //.addHeader("app_key", app_key)
                                        .build();

        Response response = client.newCall(request).execute();

        OxfordDef body = gson.fromJson(response.body().string(), OxfordDef.class);

        log.info("Response::" + body.toString());

        return body;
    }

}