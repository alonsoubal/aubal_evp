/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.model.dao.PalabrasDAO;
import root.model.entities.Palabras;

/**
 *
 * @author ANDRES
 */
@WebServlet(name = "ControllerPalabra", urlPatterns = {"/controllerPalabra"})
public class ControllerPalabra extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerPalabra</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerPalabra at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String accion = request.getParameter("accion");
        String palabra = request.getParameter("palabra");
        String significado = request.getParameter("significado");
        String fecha = request.getParameter("fecha");
        
        System.out.println("palabra" + palabra);
        System.out.println("significado" + significado);
        System.out.println("fecha" + fecha);
        
        if (accion.equalsIgnoreCase("grabarCrear")) {

            try {
                PalabrasDAO dao = new PalabrasDAO();

                Palabras pal = new Palabras();
                pal.setPalabra(palabra);
                pal.setSignificado(significado);
                pal.setFecha(Date.from(Instant.now()));
                dao.create(pal);

                List<Palabras> palabras = dao.findPalabrasEntities();
                System.out.println("Cantidad palabras en BD" + palabras.size());

                request.setAttribute("palabras", palabras);
                request.getRequestDispatcher("salida.jsp").forward(request, response);

            } catch (Exception e) {
                Logger.getLogger(ControllerPalabra.class.getName()).log(Level.SEVERE, null, e);
            }
        }

        if (accion.equalsIgnoreCase("grabarEditar")) {
            try {
                PalabrasDAO dao = new PalabrasDAO();

                Palabras pal = new Palabras();
                pal.setPalabra(palabra);
                pal.setSignificado(significado);
                pal.setFecha(Date.from(Instant.now()));
                dao.edit(pal);

                List<Palabras> palabras = dao.findPalabrasEntities();
                System.out.println("Cantidad palabras en BD" + palabras.size());

                request.setAttribute("palabras", palabras);
                request.getRequestDispatcher("salida.jsp").forward(request, response);

            } catch (Exception e) {
                Logger.getLogger(ControllerPalabra.class.getName()).log(Level.SEVERE, null, e);
            }

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
