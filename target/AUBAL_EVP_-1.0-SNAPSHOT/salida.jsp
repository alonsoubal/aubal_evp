

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="root.model.entities.Palabras"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Palabras> palabras = (List<Palabras>) request.getAttribute("palabras");
    Iterator<Palabras> itPalabras = palabras.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body class="text-center"> 
        <form name="form" action="controllerListado" method="POST">
               
        <h2>Lista de Palabras:</h2>
        <a href="index.jsp">Salir</a>
        <div class="cover-container d-flex h-100 p-3 mx-auto flex-column"><br>

            <table border="1">

                <thead>
                <th>palabras</th>
                <th>significado</th>
                <th>fecha</th>
              
                <th></th>

                </thead>
                <tbody>

                    <%while (itPalabras.hasNext()) {
                            Palabras pal = itPalabras.next();%>
                    <tr>
                        <td><%=pal.getPalabra()%></td>
                        <td><%=pal.getSignificado()%></td>
                        <td><%=pal.getFecha()%></td>
                       
                        <td> <input type="radio" name="seleccion" value="<%=pal.getPalabra()%>"></td>
                    </tr>  
                    <%}%>
                </tbody>
            </table>
            <button type="submit" name="accion" value="editar" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">Editar</button>
            <button type="submit" name="accion" value="eliminar" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">Eliminar</button>
            <button type="submit" name="accion" value="crear" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">Crear</button>
   </form>
        </body>
</html>


